import { connect } from 'react-redux';
import Filters from '../components/Map/Filters/Filters';
import { setActiveCategory } from '../actions';

const mapStateToProps = state => ({
    activeCategory: state.activeCategory,
});

const mapDispatchToProps = dispatch => ({
    setActiveCategory: id => dispatch(setActiveCategory(id)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
) (Filters);

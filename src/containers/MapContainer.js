import { connect } from 'react-redux';
import Map from '../components/Map/Map';
import { setActiveCategory } from '../actions';

const getActiveCategory = (category) => {
    return category;
};

const mapStateToProps = state => ({
    activeCategory: getActiveCategory(state.activeCategory),
});

const mapDispatchToProps = dispatch => ({
    setActiveCategory: id => dispatch(setActiveCategory(id)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
) (Map);

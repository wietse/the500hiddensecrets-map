import { SET_ACTIVE_CATEGORY } from '../constants';

const initialState = {
    activeCategory: 0,
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ACTIVE_CATEGORY:
            return { ...state, activeCategory: action.payload };

        default:
            return state;
    }
};

export default rootReducer;
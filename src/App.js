import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import MapContainer from './containers/MapContainer';
import MapList from "./components/MapList/MapList";

import './App.scss';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Switch>
                    <Route path="/map" component={MapContainer} />
                    <Route path="/maplist" component={MapList} />
                </Switch>
            </div>
        );
    }
}

export default App;

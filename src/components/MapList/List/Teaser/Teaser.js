import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Teaser.scss';

class Teaser extends Component {
    render = () => {
        return (
            <div className={"teaser" + (this.props.active ? " teaser--active" : "")} data-id={this.props.id}>
                <h2 className="teaser__title">{this.props.title}</h2>
                <p className="teaser__description">{this.props.description}</p>
                <p className="teaser__address">34 rue Yves Toudic<br/>+33 (0)1 42 40 44 52<br/>www.dupainetdesidees.com</p>
            </div>
        )
    }
}

Teaser.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    active: PropTypes.bool,
};

Teaser.defaultPropTypes = {
    active: false,
};

export default Teaser;

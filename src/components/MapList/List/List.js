import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Teaser from './Teaser/Teaser';

import './List.scss';

class List extends Component {
    constructor(props) {
        super(props);

        this.teaserId = 1;
    }

    onListScroll = () => {
        const pageYOffset = window.pageYOffset;
        let activeVenue;

        this.teasers.forEach((teaser) => {
            teaser.classList.remove('teaser--active');

            if (teaser.offsetTop < pageYOffset + teaser.clientHeight) {
                activeVenue = teaser;
                this.teaserId = teaser.getAttribute('data-id');
            }
        });

        if (activeVenue) {
            activeVenue.classList.add('teaser--active');

            if (this.teaserId) {
                this.props.activeChange(this.teaserId);
            }
        }
    };

    componentDidMount() {
        this.teasers = document.querySelectorAll('.teaser');

        window.addEventListener('scroll', this.onListScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onListScroll);
    }

    render = () => {
        return (
            <div className="list">
                {this.props.data.map((item, index) => {
                    return <Teaser
                        id={index}
                        key={index}
                        title={item.title}
                        description={item.description}
                        active={this.teaserId === item.id}
                    />;
                })}
            </div>
        )
    };
}

List.propTypes = {
    data: PropTypes.array.isRequired,
    activeChange: PropTypes.func.isRequired,
};

export default List;

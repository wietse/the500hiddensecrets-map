import React, { Component } from 'react';
import PropTypes from 'prop-types';

import MapBox from './MapBox/MapBox';

import './Map.scss';

class Map extends Component {
    render = () => {
        return (
            <div className={"map" + (this.props.fixed ? ' map--fixed' : '') + (this.props.mapStayBottom ? ' map--stay' : '')}>
                <MapBox data={this.props.data} activeVenue={this.props.activeVenue} />
            </div>
        )
    };
}

Map.propTypes = {
    data: PropTypes.array.isRequired,
    activeVenue: PropTypes.number.isRequired,
    fixed: PropTypes.bool.isRequired,
    mapStayBottom: PropTypes.bool,
};

Map.defaultPropTypes = {
    mapStayBottom: false,
};

export default Map;

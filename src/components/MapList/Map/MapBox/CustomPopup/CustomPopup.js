import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Popup } from 'react-mapbox-gl';

import './CustomPopup.scss';

class CustomPopup extends Component {
    render = () => {
        const { lat, lng, title } = this.props;

        return (
            <Popup
                coordinates={[lng, lat]}
                offset={{
                    'bottom': [0, -18],
                }}>
                <h2>{title}</h2>
                <p>Een kleine description bij de marker</p>
            </Popup>
        )
    };
}

CustomPopup.propTypes = {
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
};

export default CustomPopup;

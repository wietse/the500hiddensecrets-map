import React, { Component } from 'react';
import ReactMapboxGl, { Layer, Feature, ZoomControl } from 'react-mapbox-gl';
import PropTypes from 'prop-types';

import CustomPopup from './CustomPopup/CustomPopup';

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1Ijoid2lldHMiLCJhIjoiY2puZnFkcGRpMGZuNDN3cHNsbjdrdW1uYiJ9.MDQsfnkI2fguTJq-V_bEFA",
    scrollZoom: false,
    logoPosition: "bottom-right",
    doubleClickZoom: false,
});

class MapBox extends Component {
    render = () => {
        return (
            <Map
                style="mapbox://styles/mapbox/streets-v10" // eslint-disable-line
                containerStyle={{
                    width: "100%",
                    height: "100%",
                }}
                center={[
                    4.702852, 50.879443
                ]}
                zoom={[12]}
            >
                <Layer
                    id="locations"
                    type="circle"
                    paint={{
                        "circle-radius": 7,
                        "circle-color": "#ffffff",
                        "circle-stroke-width": 7,
                        "circle-stroke-color": "#f2858A",
                        "circle-stroke-opacity": 1
                    }}
                >
                    {this.props.data.map((item, index) => {
                        return <Feature
                            coordinates={[item.lng, item.lat]}
                            key={index}
                            properties={item}
                        />;
                    })}
                </Layer>

                <ZoomControl
                    position="top-left"
                />

                { this.props.activeVenue !== null && (<CustomPopup {...this.props.data[this.props.activeVenue]} />) }
            </Map>
        )
    };
}

MapBox.propTypes = {
    data: PropTypes.array.isRequired,
    activeVenue: PropTypes.number.isRequired,
};

export default MapBox;

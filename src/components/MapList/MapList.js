import React, { Component } from 'react';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import List from './List/List';
import Map from './Map/Map';

import './MapList.scss';

const dummyData = [
    {
        title: 'Oud begijnhof Leuven',
        description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
        lat: 50.871878,
        lng: 4.697334,
    },
    {
        title: 'De Werf',
        description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
        lat: 50.877384,
        lng: 4.702413,
    },
    {
        title: 'Boondoggle',
        description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
        lat: 50.887419,
        lng: 4.700119,
    },
    {
        title: 'Teaser title 1',
        description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
        lat: 50.886988,
        lng: 4.681975,
    },
    {
        title: 'Teaser title 2',
        description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
        lat: 50.867462,
        lng: 4.712343,
    },
];

class MapList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mapFixed: false,
            mapStayBottom: false,
            activeVenue: 1,
        }
    }

    onMapListScroll = () => {
        const pageYOffset = window.pageYOffset;
        const pageHeight = window.innerHeight;

        const mapListStickyTop = this.mapList.offsetTop;
        const mapListStickyBottom = mapListStickyTop + this.mapList.scrollHeight;

        const pageScrollPlusHeight = pageYOffset + pageHeight;

        if (pageScrollPlusHeight > mapListStickyBottom) {
            this.setState({
                mapFixed: false,
                mapStayBottom: true,
            });
        } else if (pageYOffset > mapListStickyTop) {
            this.setState({
                mapFixed: true,
                mapStayBottom: false,
            });
        } else {
            this.setState({
                mapFixed: false,
                mapStayBottom: false,
            });
        }
    };

    onListActiveChange = (activeVenue) => {
        console.log('setting');
        console.log(activeVenue);

        this.setState({
            activeVenue,
        });
    };

    componentDidMount() {
        this.mapList = document.querySelector('.map-list');

        window.addEventListener('scroll', this.onMapListScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onMapListScroll);
    }

    render = () => (
        <React.Fragment>
            <Header />

            <div className="map-list">
                <List data={dummyData} activeChange={this.onListActiveChange} />

                <Map data={dummyData} activeVenue={this.state.activeVenue} fixed={this.state.mapFixed} mapStayBottom={this.state.mapStayBottom} />
            </div>

            <Footer />
        </React.Fragment>
    )
}

export default MapList;

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Popup } from 'react-mapbox-gl';

import './CustomPopup.scss';

class CustomPopup extends Component {
    render = () => {
        const { lat, lng, title } = this.props;

        return (
            <Popup
                coordinates={[lng, lat]}
                offset={{
                    'bottom': [0, -18],
                }}
                className="custom-popup">
                <div className="custom-popup__content">
                    <div className="custom-popup__image" />

                    <div className="custom-popup__right">
                        <h2 className="custom-popup__title"><a href="https://google.be/">{title}</a></h2>

                        <div className="custom-popup__likes">667</div>
                    </div>
                </div>
            </Popup>
        )
    };
}

CustomPopup.propTypes = {
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
};

export default CustomPopup;

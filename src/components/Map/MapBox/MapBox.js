import React, { Component } from 'react';
import ReactMapboxGl, { Layer, Feature, ZoomControl } from 'react-mapbox-gl';
import PropTypes from 'prop-types';

import CustomPopup from './CustomPopup/CustomPopup';

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1Ijoid2lldHMiLCJhIjoiY2puZnFkcGRpMGZuNDN3cHNsbjdrdW1uYiJ9.MDQsfnkI2fguTJq-V_bEFA",
    logoPosition: "bottom-right",
});

class MapBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeVenue: null,
        }
    }

    onFeatureMouseEnter = (event) => {
        const { id } = event.features[0].properties;

        this.setState({
            activeVenue: id,
        });

        window.setTimeout(() => {
            this.setState({
                activeVenue: null,
            });
        }, 8000);
    };

    render = () => {
        const activeVenues = this.props.data[this.props.activeCategory].venues;

        return (
            <Map
                style="mapbox://styles/mapbox/streets-v10" // eslint-disable-line
                containerStyle={{
                    width: "60%",
                    height: "100%",
                }}
                center={[
                    4.702852, 50.879443
                ]}
                zoom={[12]}
            >
                <Layer
                    id="locations"
                    type="circle"
                    paint={{
                        "circle-radius": 7,
                        "circle-color": "#ffffff",
                        "circle-stroke-width": 7,
                        "circle-stroke-color": "#f23944",
                        "circle-stroke-opacity": 1
                    }}
                >
                    {activeVenues.map((item, index) => {
                        return <Feature
                            coordinates={[item.lng, item.lat]}
                            key={index}
                            properties={item}
                            onMouseEnter={this.onFeatureMouseEnter}
                        />;
                    })}
                </Layer>

                <ZoomControl
                    position="top-left"
                />

                { this.state.activeVenue !== null && (<CustomPopup {...activeVenues[this.state.activeVenue]} />) }
            </Map>
        )
    };
}

MapBox.propTypes = {
    data: PropTypes.array.isRequired,
    activeCategory: PropTypes.number.isRequired,
};

export default MapBox;

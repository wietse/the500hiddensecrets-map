import React, { Component } from 'react';
import PropTypes from 'prop-types';

import MapBox from './MapBox/MapBox';
import FiltersContainer from '../../containers/FiltersContainer';

import './Map.scss';

const dummyData = [
    {
        category: 'Food',
        venues: [
            {
                title: 'Oud begijnhof Leuven',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.871878,
                lng: 4.697334,
            },
            {
                title: 'De Werf',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.877384,
                lng: 4.702413,
            },
        ]
    },
    {
        category: 'Drink',
        venues: [
            {
                title: 'Boondoggle',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.937442,
                lng: 4.641089,
            },
            {
                title: 'Teaser title 1',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.943211,
                lng: 4.647477,
            },
            {
                title: 'Teaser title 2',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.982747,
                lng: 4.614626,
            },
        ]
    },
    {
        category: 'Bananenbars',
        venues: [
            {
                title: 'Boondoggle',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.900271,
                lng: 4.321992,
            },
            {
                title: 'Teaser title 1',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.905133,
                lng: 4.310044,
            },
            {
                title: 'Teaser title 2',
                description: 'This store, which dates from 1870, has managed to retain its charm with its painted glass ceilings and its bevelled mirrors. Christophe Vasseur brought this bakery to life, opening Du pain et des Idées here, which has become very popular. People come from all over Paris for the famous pain des amis, the Mouna brioche or la tendresse aux pommes. A range of authentic products, prepared in accordance with the classic French tradition. The only sour note: the bakery is closed on the weekend.',
                lat: 50.908429,
                lng: 4.313793,
            },
        ]
    }
];

class Map extends Component {
    render = () => (
        <div className="map">
            <MapBox data={dummyData} activeCategory={this.props.activeCategory} />

            <div className="map__filters">
                <FiltersContainer data={dummyData} />
            </div>
        </div>
    )
}

Map.propTypes = {
    activeCategory: PropTypes.number.isRequired,
    setActiveCategory: PropTypes.func.isRequired,
};

export default Map;

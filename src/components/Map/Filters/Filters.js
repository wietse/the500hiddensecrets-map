import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Filters.scss';

class Filters extends Component {
    onFilterClick = (clickedFilter) => {
        this.props.setActiveCategory(clickedFilter);
    };

    render = () => {
        const categories = this.props.data;
        const activeVenues = this.props.data[this.props.activeCategory].venues;

        return (
            <div className="filters">
                <h2 className="filters__title">All categories</h2>
                <ul className="filters__categories">
                    {categories.map((category, index) =>
                        <li
                            className={'filters__category' + (index === this.props.activeCategory ? ' filters__category--active' : '')}
                            key={index}
                            onClick={() => this.onFilterClick(index)}
                        >
                            {category.category}

                            {index === this.props.activeCategory &&
                                <ul className="filters__venues">
                                    {activeVenues.map((venue, index) =>
                                        <li
                                            className='filters__venue'
                                            key={index}
                                        >
                                            {venue.title}
                                        </li>
                                    )}
                                </ul>
                            }
                            </li>
                    )}
                </ul>
            </div>
        )
    };
}

Filters.propTypes = {
    activeCategory: PropTypes.number.isRequired,
    setActiveCategory: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
};

export default Filters;
